# snippet_start calc_mode
# ----- Calculation mode and parallelization ------------------------------------------------------

 CalculationMode                   = td
 ExperimentalFeatures = yes

 %Systems
   'Maxwell' | maxwell
 %

 Maxwell.ParDomains = auto
 Maxwell.ParStates  = no
# snippet_end

# snippet_start box
# ----- Maxwell box variables ---------------------------------------------------------------------

 # free maxwell box limit of 10.0 plus 2.0 for the incident wave boundaries with
 # der_order = 4 times dx_mx

 lsize_mx = 12.0
 dx_mx    = 0.5

 Maxwell.BoxShape = parallelepiped

 %Maxwell.Lsize
  lsize_mx | lsize_mx | lsize_mx
 %

 %Maxwell.Spacing
  dx_mx | dx_mx | dx_mx
 %
# snippet_end

# snippet_start calculation
# ----- Maxwell calculation variables -------------------------------------------------------------

 MaxwellHamiltonianOperator = faraday_ampere
# snippet_end

# snippet_start boundaries
 %MaxwellBoundaryConditions
  plane_waves | plane_waves | plane_waves
 %

 %MaxwellAbsorbingBoundaries
  not_absorbing | not_absorbing | not_absorbing
 %
# snippet_end

# snippet_start timestep
# ----- Time step variables -----------------------------------------------------------------------

 TDSystemPropagator = exp_mid

 timestep = 1 / ( sqrt(c^2/dx_mx^2 + c^2/dx_mx^2 + c^2/dx_mx^2) )
 TDTimeStep                        = timestep
 TDPropagationTime                 = 150*timestep
# snippet_end

# snippet_start output
# ----- Output variables --------------------------------------------------------------------------

 OutputFormat = plane_x + plane_y + plane_z + axis_x + axis_y + axis_z

# ----- Maxwell output variables ------------------------------------------------------------------

 %MaxwellOutput
  electric_field 
  magnetic_field 
  maxwell_energy_density 
  trans_electric_field
 %

 MaxwellOutputInterval             = 50
 MaxwellTDOutput                   = maxwell_energy + maxwell_total_e_field + maxwell_total_b_field
# snippet_end

# snippet_start field
# ----- Maxwell field variables -------------------------------------------------------------------

 # laser propagates in x direction

 lambda1 = 10.0
 omega1  = 2 * pi * c / lambda1
 k1_x    = omega1 / c
 E1_z    = 0.05
 pw1     = 10.0
 ps1_x   = - 25.0

 %MaxwellIncidentWaves
   plane_wave_mx_function | 0 | 0 | E1_z | "plane_waves_function_1"
 %

 %MaxwellFunctions
   "plane_waves_function_1" | mxf_cosinoidal_wave | k1_x | 0    | 0 | ps1_x | 0     | 0 | pw1
 %
# snippet_end